﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MrDailyReport.Startup))]
namespace MrDailyReport
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
