﻿using MrDailyReport.IRepository;
using MrDailyReport.Models;
using MrDailyReport.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using MrDailyReport.Models;
namespace MrDailyReport.Controllers
{
    public class HomeController : Controller
    {
        //private ICommon<LoginDtl> repository = null;
        //private ICommon<RegistrationDtl> regrepository = null;
        //public HomeController()
        //{
        //    this.repository = new Common<LoginDtl>();
        //    this.regrepository = new Common<RegistrationDtl>();

        //}

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        //public ActionResult GetReg(int id)
        //{
        //    var data = repository.GetById(id);
        //    regrepository.Save();
        //    return View();
        //}

        //public ActionResult GetAllReg()
        //{
        //    repository.GetAll();
        //    return View();
        //}
        //public ActionResult InsertReg(RegistrationDtl reg)
        //{
        //    regrepository.Insert(reg);
        //    return View();
        //}

        //public ActionResult Updatereg(RegistrationDtl reg)
        //{
        //    regrepository.Update(reg);
        //    return View();
        //}

        //public ActionResult Delete(int id)
        //{
        //    regrepository.Delete(id);
        //    return View();
        //}

       public ActionResult Home()
        {
            return View();
        }

        public ActionResult Services()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel model )
        {
            MRDailyReporingEntities db = new MRDailyReporingEntities();
            var s = db.GetCBLoginInfo(model.UserName, model.Password);

            var item = s.ToString();
            if (item == "Success")
            {

                return View("UserLandingView");
            }
            else if (item == "User Does not Exists")

            {
                ViewBag.NotValidUser = item;

            }
            else
            {
                ViewBag.Failedcount = item;
            }

            return View("Login");
            
        }

        public ActionResult Registration()
        {
            return View();
        }

    }
}